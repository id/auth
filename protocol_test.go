package auth

import (
	"testing"

	"github.com/go-webauthn/webauthn/protocol"
	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/google/go-cmp/cmp"

	"git.autistici.org/id/usermetadb"
)

func TestProtocol_SerializeRequest(t *testing.T) {
	c := &kvCodec{}

	req := &Request{
		Service:  "service",
		Username: "username",
		Password: []byte("password"),
		OTP:      "123456",
		WebAuthnSession: &webauthn.SessionData{
			Challenge: "challenge",
			UserID:    []byte("user_id"),
		},
		// Note: a lot of the following fields are set to something
		// otherwise they will receive values on deserialization.
		WebAuthnResponse: &protocol.ParsedCredentialAssertionData{
			ParsedPublicKeyCredential: protocol.ParsedPublicKeyCredential{
				ParsedCredential: protocol.ParsedCredential{
					ID:   "keyid",
					Type: "ecc256",
				},
			},
			Raw: protocol.CredentialAssertionResponse{
				PublicKeyCredential: protocol.PublicKeyCredential{
					RawID: protocol.URLEncodedBase64{0x9e, 0xe9, 0x65},
				},
				AssertionResponse: protocol.AuthenticatorAssertionResponse{
					AuthenticatorResponse: protocol.AuthenticatorResponse{ClientDataJSON: protocol.URLEncodedBase64{0x9e, 0xe9, 0x65}},
					AuthenticatorData:     protocol.URLEncodedBase64{0x9e, 0xe9, 0x65},
					Signature:             protocol.URLEncodedBase64{0x9e, 0xe9, 0x65},
				},
			},
		},
		DeviceInfo: &usermetadb.DeviceInfo{
			ID:         "ah9hf3kh2",
			RemoteAddr: "1.2.3.4",
			RemoteZone: "cc",
			Browser:    "IE9",
			OS:         "Windows",
			UserAgent:  "MSIE/9.1 foo/12 bar/13",
			Mobile:     true,
		},
	}

	b := c.Encode(req)

	var req2 Request
	err := c.Decode(b, &req2)
	if err != nil {
		t.Fatal("Decode():", err)
	}
	if diffs := cmp.Diff(req, &req2); diffs != "" {
		t.Errorf("decode results differ:\n%s", diffs)
	}
}

func TestProtocol_SerializeResponse(t *testing.T) {
	c := &kvCodec{}

	chal, _ := protocol.CreateChallenge()

	resp := &Response{
		Status:     StatusInsufficientCredentials,
		Mechanism:  MechanismWebAuthN,
		TFAMethods: []TFAMethod{TFAMethodU2F},
		WebAuthnData: &protocol.CredentialAssertion{
			protocol.PublicKeyCredentialRequestOptions{
				Challenge:        chal,
				Timeout:          100,
				RelyingPartyID:   "rpid",
				UserVerification: "none",
			},
		},
		UserInfo: &UserInfo{
			Email:  "test@example.com",
			Shard:  "42",
			Groups: []string{"group1", "group2", "group3"},
		},
	}

	b := c.Encode(resp)

	var resp2 Response
	err := c.Decode(b, &resp2)
	if err != nil {
		t.Fatal("Decode():", err)
	}
	if diffs := cmp.Diff(resp, &resp2); diffs != "" {
		t.Errorf("decode results differ: %s", diffs)
	}
}
