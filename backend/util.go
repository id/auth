package backend

import (
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

// LoadYAML loads and unmarshals a YAML file.
func LoadYAML(path string, obj interface{}) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return yaml.NewDecoder(f).Decode(obj)
}

// ResolvePath returns the path evaluated as relative to base.
func ResolvePath(path, base string) string {
	if !filepath.IsAbs(path) {
		path = filepath.Join(base, path)
	}
	return path
}
