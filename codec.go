package auth

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
)

// Codec serializes simple key/value maps on the wire. Requirements
// for a codec: it must transfer maps, it must be trivial to implement
// in both C, Go and Python. It must not have messy external
// dependencies if possible.
type Codec interface {
	Encode(interface{}) []byte
	Decode([]byte, interface{}) error
}

type mapEncodable interface {
	EncodeToMap(map[string]string, string)
}

type mapDecodable interface {
	DecodeFromMap(map[string]string, string)
}

type kvCodec struct{}

// DefaultCodec is the codec used by the on-wire protocol.
var DefaultCodec = &kvCodec{}

//var charsToEscape = "\" \r\n"

func shouldEscapeString(s string) bool {
	for _, r := range s {
		if r < 32 || r > 127 || r == '"' || r == ' ' || r == '\r' || r == '\n' {
			return true
		}
	}
	return false
}

func (c kvCodec) encodeMap(obj map[string]string) []byte {
	var buf bytes.Buffer
	first := true
	for key, value := range obj {
		if first {
			first = false
		} else {
			io.WriteString(&buf, " ")
		}
		fmt.Fprintf(&buf, "%s=", key)
		if shouldEscapeString(value) {
			w := base64.NewEncoder(base64.RawURLEncoding, &buf)
			io.WriteString(w, value)
			w.Close()
		} else {
			fmt.Fprintf(&buf, "\"%s\"", value)
		}
	}
	return buf.Bytes()
}

func (c kvCodec) Encode(obj interface{}) []byte {
	var m map[string]string
	switch t := obj.(type) {
	case map[string]string:
		m = t
	case mapEncodable:
		m = make(map[string]string)
		t.EncodeToMap(m, "")
	default:
		// TODO: Error
	}
	return c.encodeMap(m)
}

type inputScanner struct {
	b   []byte
	pos int
}

func (i *inputScanner) next() (byte, bool) {
	if i.pos >= len(i.b) {
		return 0, true
	}
	value := i.b[i.pos]
	i.pos++
	return value, false
}

func (i *inputScanner) ungetc() {
	if i.pos > 0 {
		i.pos--
	}
}

func (i *inputScanner) skipWhitespace() {
	for {
		c, eof := i.next()
		if eof {
			return
		}
		if c != ' ' {
			break
		}
	}
	i.ungetc()
}

func (i *inputScanner) parseUntil(sep byte) ([]byte, error) {
	pos := i.pos
	idx := bytes.IndexByte(i.b[pos:], sep)
	if idx == -1 {
		return nil, io.EOF
	}
	value := i.b[i.pos : i.pos+idx]
	i.pos += idx + 1
	return value, nil
}

func (i *inputScanner) parseUntilOrEOF(sep byte) []byte {
	pos := i.pos
	idx := bytes.IndexByte(i.b[pos:], sep)
	if idx == -1 {
		i.pos = len(i.b)
	} else {
		i.pos = pos + idx
	}
	return i.b[pos:i.pos]
}

func (i *inputScanner) parseKey() (string, error) {
	i.skipWhitespace()
	key, err := i.parseUntil('=')
	if err != nil {
		return "", err
	}
	return string(key), nil
}

func (i *inputScanner) parseQuotedString() (string, error) {
	s, err := i.parseUntil('"')
	if err != nil {
		return "", err
	}
	return string(s), nil
}

func (i *inputScanner) parseBase64String() (string, error) {
	data := bytes.TrimRight(i.parseUntilOrEOF(' '), "=")
	out := make([]byte, base64.RawURLEncoding.DecodedLen(len(data)))
	_, err := base64.RawURLEncoding.Decode(out, data)
	if err != nil {
		return "", err
	}
	return string(out), nil
}

func (i *inputScanner) parseValue() (string, error) {
	c, eof := i.next()
	if eof {
		return "", io.EOF
	}
	if c == '"' {
		return i.parseQuotedString()
	}
	i.ungetc()
	return i.parseBase64String()
}

func (i *inputScanner) parseAssignment() (string, string, error) {
	key, err := i.parseKey()
	if err == io.EOF {
		return "", "", nil
	} else if err != nil {
		return "", "", err
	}
	value, err := i.parseValue()
	if err != nil {
		return "", "", err
	}
	return key, value, nil
}

func (c kvCodec) decodeMap(b []byte, out map[string]string) (map[string]string, error) {
	if out == nil {
		out = make(map[string]string)
	}
	i := &inputScanner{b: b}
	for {
		key, value, err := i.parseAssignment()
		if err != nil {
			return nil, err
		}
		if key == "" {
			break
		}
		out[key] = value
	}
	return out, nil
}

func (c kvCodec) Decode(b []byte, obj interface{}) error {
	var m map[string]string
	switch t := obj.(type) {
	case map[string]string:
		m = t
	}
	var err error
	m, err = c.decodeMap(b, m)
	if err != nil {
		return err
	}
	switch t := obj.(type) {
	case mapDecodable:
		t.DecodeFromMap(m, "")
	}
	return nil
}
