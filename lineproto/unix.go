package lineproto

import (
	"errors"
	"net"
	"os"

	"github.com/gofrs/flock"
)

type socketListener struct {
	net.Listener

	lock *flock.Flock
}

func (l *socketListener) Close() error {
	defer l.lock.Unlock() // nolint: errcheck
	return l.Listener.Close()
}

// NewUNIXSocketListener creates a new net.Listener listening on a
// UNIX socket at the given path, with a filesystem-level lock.
func NewUNIXSocketListener(socketPath string) (net.Listener, error) {
	// The simplest workflow is: create a new socket, remove it on
	// exit. However, if the program crashes, the socket might
	// stick around and prevent the next execution from starting
	// successfully. We could remove it before starting, but that
	// would be dangerous if another instance was listening on
	// that socket. So we wrap socket access with a file lock.
	lock := flock.New(socketPath + ".lock")
	locked, err := lock.TryLock()
	if err != nil {
		return nil, err
	}
	if !locked {
		return nil, errors.New("socket is locked by another process")
	}

	addr, err := net.ResolveUnixAddr("unix", socketPath)
	if err != nil {
		lock.Unlock()
		return nil, err
	}

	// Always try to unlink the socket before creating it.
	os.Remove(socketPath) // nolint

	l, err := net.ListenUnix("unix", addr)
	if err != nil {
		lock.Unlock()
		return nil, err
	}

	return &socketListener{Listener: l, lock: lock}, nil
}
