package server

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/pquerna/otp/totp"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/client"
	"git.autistici.org/id/usermetadb"
)

type testServer struct {
	tmpdir string
	srv    *Server
}

func createTestServer(t testing.TB, configFiles map[string]string) *testServer {
	tmpdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	for name, content := range configFiles {
		if err = ioutil.WriteFile(filepath.Join(tmpdir, name), []byte(content), 0600); err != nil {
			t.Fatal(err)
		}
	}

	config, err := LoadConfig(filepath.Join(tmpdir, "config.yml"))
	if err != nil {
		t.Fatal("LoadConfig():", err)
	}

	srv, err := NewServer(config)
	if err != nil {
		t.Fatal("NewServer():", err)
	}

	return &testServer{
		tmpdir: tmpdir,
		srv:    srv,
	}
}

func (s *testServer) Close() {
	s.srv.Close()
	_ = os.RemoveAll(s.tmpdir)
}

// A small adapter to make Server conform to the Client interface
// (Authenticate needs to return an error).
type clientAdapter struct {
	*Server
}

func (c *clientAdapter) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	return c.Server.Authenticate(ctx, req), nil
}

var (
	testUsersFileStr = `---
- name: testuser
  email: testuser@example.com
  password: "$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b"
  groups:
    - group1
    - group2

- name: 2fauser
  email: 2fauser@example.com
  shard: 42
  password: "$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b"
  totp_secret: "O32OBVS5BL5EAPB5"
  u2f_registrations:
    - key_handle: "JcolXA6KaoihO8VuxSugtCT5jyh-6lFuWXLkFAPe8s9qszxTMvDAtJn8gmYg9uGO-kmjgap1h0llchlqqjCpKw"
      public_key: "0498ee4565cd348031cf36ee3549b63b5ea23b5e7ea6f297e7cccaeba99983d185110fb94fa6455c82d3e5c8d0be10be71308d76062fb5fa50d3ea8228048f0037"

- name: aspuser
  email: aspuser@example.com
  shard: 42
  password: "$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b"
  app_specific_passwords:
    - service: test
      password: "$s$16384$8$1$5b8b0dc22ec2bc1029b06d4856a8b471$6c7257652e3982d8729b760885887b18d4a28966f9b2364041ddada8bea297db"
    - service: force2fa
      password: "$s$16384$8$1$5b8b0dc22ec2bc1029b06d4856a8b471$6c7257652e3982d8729b760885887b18d4a28966f9b2364041ddada8bea297db"
`

	webAuthnConfigStr = `
webauthn:
  rp_display_name: "Example.Com"
  rp_id: "example.com"
  rp_origin: "https://example.com"
`

	testConfigStr = `---
services:
  test:
    backends:
      - backend: file
        params:
          src: users.yml
  no2fa:
    ignore_2fa: true
    backends:
      - backend: file
        params:
          src: users.yml
  force2fa:
    enforce_2fa: true
    backends:
      - backend: file
        params:
          src: users.yml
  interactive:
    challenge_response: true
    backends:
      - backend: file
        params:
          src: users.yml
` + webAuthnConfigStr

	testConfigStrWithStaticGroups = `---
services:
  test:
    backends:
      - backend: file
        params:
          src: users.yml
        static_groups: [testgroup]
  no2fa:
    ignore_2fa: true
    backends:
      - backend: file
        params:
          src: users.yml
  force2fa:
    enforce_2fa: true
    backends:
      - backend: file
        params:
          src: users.yml
  interactive:
    challenge_response: true
    backends:
      - backend: file
        params:
          src: users.yml
` + webAuthnConfigStr

	testConfigStrWithRatelimit = `---
services:
  test:
    backends:
      - backend: file
        params:
          src: users.yml
    rate_limits:
      - failed_ip_bl
      - failed_login_bl
rate_limits:
  failed_login_bl:
    limit: 10
    period: 300
    blacklist_for: 3600
    on_failure: true
    keys: [user]
  failed_ip_bl:
    limit: 10
    period: 300
    blacklist_for: 3600
    on_failure: true
    keys: [ip]
` + webAuthnConfigStr
)

func runBackendTest(t *testing.T, srv *Server) {
	testdata := []struct {
		username, service                         string
		expectGroups                              []string
		expectHasOTP, expectHasU2F, expectHasASPs bool
	}{
		{"testuser", "test", nil, false, false, false},
		{"2fauser", "test", nil, true, true, false},
		{"aspuser", "test", nil, false, false, true},
	}
	for _, td := range testdata {
		svc, ok := srv.getService(td.service)
		if !ok {
			t.Errorf("no such service: %s", td.service)
			continue
		}
		user, ok := srv.getUser(context.Background(), svc, td.username)
		if !ok {
			t.Errorf("no such user: %s", td.username)
			continue
		}
		if b := user.HasOTP(); b != td.expectHasOTP {
			t.Errorf("user %s has_otp=%v, expected=%v", td.username, b, td.expectHasOTP)
		}
		if b := user.HasU2F(); b != td.expectHasU2F {
			t.Errorf("user %s has_u2f=%v, expected=%v", td.username, b, td.expectHasU2F)
		}
		if b := user.HasASPs(td.service); b != td.expectHasASPs {
			t.Errorf("user %s has_asp=%v, expected=%v", td.username, b, td.expectHasASPs)
		}
	}
}

func runAuthenticationTest(t *testing.T, client client.Client) {
	// Test a number of simple password logins.
	testdata := []struct {
		service, username, password string
		expectedStatus              auth.Status
	}{
		{"test", "testuser", "password", auth.StatusOK},
		{"bad_service", "testuser", "password", auth.StatusError},
		{"test", "bad_user", "password", auth.StatusError},
		{"test", "testuser", "bad_password", auth.StatusError},
		{"test", "2fauser", "password", auth.StatusError},
		{"test", "aspuser", "password", auth.StatusError},
		{"test", "aspuser", "password2", auth.StatusOK},

		{"no2fa", "testuser", "password", auth.StatusOK},
		{"no2fa", "2fauser", "password", auth.StatusOK},
		{"no2fa", "aspuser", "password", auth.StatusOK},

		{"force2fa", "testuser", "password", auth.StatusError},
		{"force2fa", "2fauser", "password", auth.StatusError},
		{"force2fa", "aspuser", "password", auth.StatusError},
		{"force2fa", "aspuser", "password2", auth.StatusOK},
	}
	for _, td := range testdata {
		resp, err := client.Authenticate(context.Background(), &auth.Request{
			Service:  td.service,
			Username: td.username,
			Password: []byte(td.password),
		})
		if err != nil {
			t.Errorf("transport error: %v", err)
			continue
		}
		if resp.Status != td.expectedStatus {
			t.Errorf("authentication error: s=%s u=%s p=%s, expected=%v got=%v", td.service, td.username, td.password, td.expectedStatus, resp.Status)
		}
	}

	// Test OTP access to an interactive service.
	validOTP, _ := totp.GenerateCode("O32OBVS5BL5EAPB5", time.Now())
	testdata2 := []struct {
		username, password, otp string
		expectedStatus          auth.Status
		expectedTFAMethod       auth.TFAMethod
	}{
		{"testuser", "password", "", auth.StatusOK, auth.TFAMethodNone},
		{"2fauser", "bad_password", "", auth.StatusError, auth.TFAMethodNone},
		{"2fauser", "bad_password", validOTP, auth.StatusError, auth.TFAMethodNone},
		{"2fauser", "password", "", auth.StatusInsufficientCredentials, auth.TFAMethodOTP},
		{"2fauser", "password", validOTP, auth.StatusOK, auth.TFAMethodNone},
		{"2fauser", "password", validOTP, auth.StatusError, auth.TFAMethodNone}, // fails due to replay protection
		{"2fauser", "password", "123456", auth.StatusError, auth.TFAMethodNone},
	}
	for _, td := range testdata2 {
		resp, err := client.Authenticate(context.Background(), &auth.Request{
			Service:  "interactive",
			Username: td.username,
			OTP:      td.otp,
			Password: []byte(td.password),
		})
		if err != nil {
			t.Errorf("transport error: %v", err)
			continue
		}
		if resp.Status != td.expectedStatus {
			t.Errorf("authentication error: s=interactive u=%s p=%s, expected=%v got=%v", td.username, td.password, td.expectedStatus, resp.Status)
		}
		if td.expectedTFAMethod != auth.TFAMethodNone && !resp.Has2FAMethod(td.expectedTFAMethod) {
			t.Errorf("mismatch in TFAMethod hint in authentication response: s=interactive u=%s p=%s, expected=%v got=%v", td.username, td.password, td.expectedTFAMethod, resp.TFAMethods)
		}
	}
}

func TestAuthServer(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()
	runAuthenticationTest(t, &clientAdapter{s.srv})
}

func TestAuthServer_Backend(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()
	runBackendTest(t, s.srv)
}

func TestAuthServer_Blacklist(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStrWithRatelimit,
	})
	defer s.Close()
	c := &clientAdapter{s.srv}

	// Trigger the failed login blacklist, then verify that the
	// user is blacklisted even when trying with the right password.
	for i := 0; i < 100; i++ {
		c.Authenticate(context.Background(), &auth.Request{
			Service:  "test",
			Username: "testuser",
			Password: []byte("bad_password"),
		})
	}
	resp, _ := c.Authenticate(context.Background(), &auth.Request{
		Service:  "test",
		Username: "testuser",
		Password: []byte("password"),
	})
	if resp.Status != auth.StatusError {
		t.Fatalf("user was not blacklisted: %v", resp)
	}
}

func TestAuthServer_Blacklist_UnknownUser(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStrWithRatelimit,
	})
	defer s.Close()
	c := &clientAdapter{s.srv}

	// Trigger the failed IP blacklist.
	for i := 0; i < 100; i++ {
		c.Authenticate(context.Background(), &auth.Request{
			Service:  "test",
			Username: fmt.Sprintf("nonexistinguser%d", i),
			Password: []byte("bad_password"),
			DeviceInfo: &usermetadb.DeviceInfo{
				RemoteAddr: "1.2.3.4",
			},
		})
	}
	// Reach through the internals to verify that the IP is in
	// the blacklist.
	if _, ok := s.srv.services["test"].bl[0].bl.bl["1.2.3.4"]; !ok {
		t.Fatalf("IP was not blacklisted")
	}
}

func TestAuthServer_Blacklist_BelowLimit(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStrWithRatelimit,
	})
	defer s.Close()
	c := &clientAdapter{s.srv}

	// A small number of failures should not trigger the blacklist.
	for i := 0; i < 8; i++ {
		c.Authenticate(context.Background(), &auth.Request{
			Service:  "test",
			Username: "testuser",
			Password: []byte("bad_password"),
		})
	}
	resp, _ := c.Authenticate(context.Background(), &auth.Request{
		Service:  "test",
		Username: "testuser",
		Password: []byte("password"),
	})
	if resp.Status != auth.StatusOK {
		t.Fatal("user was incorrectly blacklisted")
	}
}

func TestAuthServer_WithStaticGroups(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStrWithStaticGroups,
	})
	defer s.Close()
	c := &clientAdapter{s.srv}

	resp, _ := c.Authenticate(context.Background(), &auth.Request{
		Service:  "test",
		Username: "testuser",
		Password: []byte("password"),
	})
	if resp.Status != auth.StatusOK {
		t.Fatal("auth failed unexpectedly")
	}

	// testgroup should only appear once.
	var n int
	for _, g := range resp.UserInfo.Groups {
		if g == "testgroup" {
			n++
		}
	}
	if n != 1 {
		t.Fatalf("bad group membership: %v", resp.UserInfo.Groups)
	}
}

func TestAuthServer_File_HasU2F(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()

	// Check that the user U2F registrations were decoded successfully.
	svc, _ := s.srv.getService("test")
	u, ok := s.srv.getUser(context.Background(), svc, "2fauser")
	if !ok {
		t.Fatal("user not found")
	}
	if len(u.WebAuthnRegistrations) != 1 {
		t.Fatalf("found %d WebAuthN registrations, expecting 1", len(u.WebAuthnRegistrations))
	}
}
