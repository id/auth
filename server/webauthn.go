package server

import (
	"git.autistici.org/id/auth/backend"
	"github.com/go-webauthn/webauthn/webauthn"
)

type webauthnUser struct {
	*backend.User
}

func (u *webauthnUser) WebAuthnID() []byte {
	return []byte(u.Name)
}

func (u *webauthnUser) WebAuthnName() string {
	return u.Name
}

func (u *webauthnUser) WebAuthnDisplayName() string {
	return u.Name
}

func (u *webauthnUser) WebAuthnIcon() string { return "" }

func (u *webauthnUser) WebAuthnCredentials() []webauthn.Credential {
	return u.WebAuthnRegistrations
}

func newWebAuthnUser(user *backend.User) *webauthnUser {
	return &webauthnUser{User: user}
}
