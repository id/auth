package server

import (
	"context"
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/client"
	"git.autistici.org/id/auth/lineproto"
)

func newSocketServerWithPath(t testing.TB, path string, h lineproto.LineHandler) func() {
	l, err := lineproto.NewUNIXSocketListener(path)
	if err != nil {
		t.Fatal(err)
	}

	lsrv := lineproto.NewLineServer(h)
	srv := lineproto.NewServer("test", l, lsrv)

	go srv.Serve()
	return func() {
		srv.Close()
		os.Remove(path)
	}
}

func newSocketServer(t testing.TB, h lineproto.LineHandler) (string, func()) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	path := filepath.Join(dir, "socket")
	cleanup := newSocketServerWithPath(t, path, h)
	return path, func() {
		cleanup()
		os.RemoveAll(dir)
	}
}

func TestAuthServer_UNIX(t *testing.T) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()

	sock, cleanup := newSocketServer(t, NewSocketServer(s.srv))
	defer cleanup()

	c := client.New(sock)
	runAuthenticationTest(t, c)
}

func TestAuthServer_UNIX_ReuseSocket(t *testing.T) {
	// Leave a stray socket around.
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	sock := filepath.Join(dir, "socket")
	addr, _ := net.ResolveUnixAddr("unix", sock)
	l, _ := net.ListenUnix("unix", addr)
	l.SetUnlinkOnClose(false)
	l.Close()

	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()

	cleanup := newSocketServerWithPath(t, sock, NewSocketServer(s.srv))
	defer cleanup()

	// Verify that a request does not return a connection error
	// (regardless of the response).
	c := client.New(sock)
	_, err = c.Authenticate(context.Background(), &auth.Request{Username: "foo"})
	if err != nil {
		t.Fatal("Authenticate():", err)
	}
}

func runMany(t testing.TB, concurrency, count int, f func(string) error) {
	s := createTestServer(t, map[string]string{
		"users.yml":  testUsersFileStr,
		"config.yml": testConfigStr,
	})
	defer s.Close()

	sock, cleanup := newSocketServer(t, NewSocketServer(s.srv))
	defer cleanup()

	var wg sync.WaitGroup
	for n := 0; n < concurrency; n++ {
		wg.Add(1)
		go func(n int) {
			defer wg.Done()
			for i := 0; i < count; i++ {
				if err := f(sock); err != nil {
					t.Errorf("thread %d, iteration %d: %v", n, i, err)
				}
				time.Sleep(1 * time.Millisecond)
			}
		}(n)
	}
	wg.Wait()
}

func spewRandomBytes(socketPath string) error {
	c, err := net.Dial("unix", socketPath)
	if err != nil {
		return err
	}

	defer c.Close()

	var randBytes [1024]byte
	rand.Read(randBytes[:])
	c.Write(randBytes[:])
	c.Write([]byte("\r\n"))
	return nil
}

func TestAuthServer_UNIX_SyntaxError(t *testing.T) {
	// Send random crap, see that it does not crash.
	runMany(t, 10, 100, spewRandomBytes)
}

func trySuccessfulLogin(socketPath string) error {
	c := client.New(socketPath)
	//defer c.Close()
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	resp, err := c.Authenticate(ctx, &auth.Request{
		Service:  "test",
		Username: "testuser",
		Password: []byte("password"),
	})
	if err != nil {
		return err
	}
	if resp.Status != auth.StatusOK {
		return fmt.Errorf("unexpected status %s", resp.Status.String())
	}
	return nil
}

func TestAuthServer_UNIX_ConcurrentLogins(t *testing.T) {
	runMany(t, 10, 3, trySuccessfulLogin)
}

func BenchmarkAuthServer_UNIX_ConcurrentLogins(b *testing.B) {
	// This basically measures scrypt performance.
	runMany(b, 4, b.N, trySuccessfulLogin)
}
