package server

import (
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"git.autistici.org/id/auth"
	"github.com/prometheus/client_golang/prometheus"
)

type ratelimitDatum struct {
	stamp   int64
	counter uint64
}

func (d ratelimitDatum) age(now int64) int64 {
	return now - d.stamp
}

// Ratelimiter is a simple counter-based rate limiter, allowing the
// first N requests over each period of time T.
type Ratelimiter struct {
	name   string
	limit  uint64
	period int64

	mx sync.Mutex
	c  map[string]ratelimitDatum
}

func newRatelimiter(name string, limit, period int) *Ratelimiter {
	r := &Ratelimiter{
		name:   name,
		limit:  uint64(limit),
		period: int64(period),
		c:      make(map[string]ratelimitDatum),
	}
	go r.expungeThread()
	return r
}

// AllowIncr performs a check and an increment at the same time, while
// holding a mutex, so it is robust in face of concurrent requests.
func (r *Ratelimiter) AllowIncr(key string) bool {
	if key == "" {
		return true
	}
	r.mx.Lock()
	d := r.get(key)
	var allowed bool
	if d.counter <= r.limit {
		allowed = true
		d.counter++
		r.set(key, d)
	}
	r.mx.Unlock()

	return allowed
}

func (r *Ratelimiter) get(key string) ratelimitDatum {
	now := time.Now().Unix()
	d, ok := r.c[key]
	if !ok || d.age(now) > r.period {
		d = ratelimitDatum{stamp: now}
		r.c[key] = d
	}
	return d
}

func (r *Ratelimiter) set(key string, d ratelimitDatum) {
	r.c[key] = d
}

func (r *Ratelimiter) expunge() {
	cutoff := time.Now().Unix() - 2*r.period
	r.mx.Lock()
	for k, d := range r.c {
		if d.stamp < cutoff {
			delete(r.c, k)
		}
	}
	r.mx.Unlock()
}

var ratelimitExpungePeriod = 300 * time.Second

func (r *Ratelimiter) expungeThread() {
	for range time.NewTicker(ratelimitExpungePeriod).C {
		r.expunge()
	}
}

// Blacklist can blacklist keys whose request rate is above a
// specified threshold.
type Blacklist struct {
	name   string
	r      *Ratelimiter
	bl     map[string]int64
	blTime int64
}

func newBlacklist(name string, limit, period, blacklistTime int) *Blacklist {
	return &Blacklist{
		name:   name,
		r:      newRatelimiter(name, limit, period),
		bl:     make(map[string]int64),
		blTime: int64(blacklistTime),
	}
}

// Allow returns true if this request (identified by the given key)
// should be allowed.
func (b *Blacklist) Allow(key string) bool {
	if key == "" {
		return true
	}

	b.r.mx.Lock()
	deadline, ok := b.bl[key]
	if ok && deadline < time.Now().Unix() {
		delete(b.bl, key)
		ok = false
	}
	b.r.mx.Unlock()
	return !ok
}

// Incr increments the counter for the given key for the current time
// period.
func (b *Blacklist) Incr(key string) {
	if key == "" {
		return
	}

	// Count one higher than limit, and trigger the blacklist when
	// we reach that.
	limitp1 := b.r.limit + 1

	b.r.mx.Lock()
	d := b.r.get(key)
	if d.counter < limitp1 {
		d.counter++
		b.r.set(key, d)
	} else if d.counter == limitp1 {
		log.Printf("blacklisted %s (%s)", key, b.name)
		blacklistCounter.WithLabelValues(b.name).Inc()
		b.bl[key] = time.Now().Unix() + b.blTime
	}
	b.r.mx.Unlock()
}

// Function that extracts a key from a request.
type ratelimitKeyFunc func(string, *auth.Request) string

// Extract the username from the request.
func usernameKey(username string, _ *auth.Request) string {
	return username
}

// Extract the client IP address (if present) from the request.
func ipAddrKey(_ string, req *auth.Request) string {
	if req.DeviceInfo != nil {
		return req.DeviceInfo.RemoteAddr
	}
	return ""
}

func parseKeyFunc(k string) (ratelimitKeyFunc, error) {
	switch k {
	case "ip":
		return ipAddrKey, nil
	case "user":
		return usernameKey, nil
	default:
		return nil, fmt.Errorf("unknown key %s", k)
	}
}

// Configuration for a rate limiter or blacklist (depending on whether
// BlacklistTime is set or not). All times are specified in seconds.
type authRatelimiterConfig struct {
	Limit         int      `yaml:"limit"`
	Period        int      `yaml:"period"`
	BlacklistTime int      `yaml:"blacklist_for"`
	OnFailure     bool     `yaml:"on_failure"`
	Keys          []string `yaml:"keys"`
	Bypass        []struct {
		Key   string `yaml:"key"`
		Value string `yaml:"value"`
	} `yaml:"bypass"`
}

type bypassEvaluator struct {
	keyFn ratelimitKeyFunc
	value string
}

func (r *authRatelimiterConfig) bypassFunctions() ([]bypassEvaluator, error) {
	out := make([]bypassEvaluator, 0, len(r.Bypass))
	for _, b := range r.Bypass {
		f, err := parseKeyFunc(b.Key)
		if err != nil {
			return nil, err
		}
		out = append(out, bypassEvaluator{
			keyFn: f,
			value: b.Value,
		})
	}
	return out, nil
}

func (r *authRatelimiterConfig) keyFunctions() ([]ratelimitKeyFunc, error) {
	var keyFuncs []ratelimitKeyFunc
	for _, k := range r.Keys {
		f, err := parseKeyFunc(k)
		if err != nil {
			return nil, err
		}
		keyFuncs = append(keyFuncs, f)
	}
	return keyFuncs, nil
}

const rlKeySep = ";"

type authRatelimiterBase struct {
	keyFuncs []ratelimitKeyFunc
	bypass   []bypassEvaluator
}

func newAuthRatelimiterBase(config *authRatelimiterConfig) (*authRatelimiterBase, error) {
	kf, err := config.keyFunctions()
	if err != nil {
		return nil, err
	}
	be, err := config.bypassFunctions()
	if err != nil {
		return nil, err
	}
	return &authRatelimiterBase{
		keyFuncs: kf,
		bypass:   be,
	}, nil
}

func (r *authRatelimiterBase) shouldBypass(username string, req *auth.Request) bool {
	for _, b := range r.bypass {
		if b.value == b.keyFn(username, req) {
			return true
		}
	}
	return false
}

func (r *authRatelimiterBase) key(username string, req *auth.Request) string {
	if len(r.keyFuncs) == 1 {
		return r.keyFuncs[0](username, req)
	}

	var parts []string
	for _, f := range r.keyFuncs {
		parts = append(parts, f(username, req))
	}
	return strings.Join(parts, rlKeySep)
}

// Request-oriented ratelimiter with configurable keys.
type authRatelimiter struct {
	*authRatelimiterBase
	rl *Ratelimiter
}

func newAuthRatelimiter(name string, config *authRatelimiterConfig) (*authRatelimiter, error) {
	r, err := newAuthRatelimiterBase(config)
	if err != nil {
		return nil, err
	}
	return &authRatelimiter{
		authRatelimiterBase: r,
		rl:                  newRatelimiter(name, config.Limit, config.Period),
	}, nil
}

func (r *authRatelimiter) AllowIncr(username string, req *auth.Request) bool {
	if r.shouldBypass(username, req) {
		return true
	}
	key := r.key(username, req)
	if key == "" { // An empty key bypasses the rate limit.
		return true
	}
	return r.rl.AllowIncr(key)
}

// Request-oriented blacklist with configurable keys.
type authBlacklist struct {
	*authRatelimiterBase
	bl        *Blacklist
	onFailure bool
}

func newAuthBlacklist(name string, config *authRatelimiterConfig) (*authBlacklist, error) {
	r, err := newAuthRatelimiterBase(config)
	if err != nil {
		return nil, err
	}
	return &authBlacklist{
		authRatelimiterBase: r,
		bl:                  newBlacklist(name, config.Limit, config.Period, config.BlacklistTime),
		onFailure:           config.OnFailure,
	}, nil
}

func (b *authBlacklist) Allow(username string, req *auth.Request) bool {
	if b.shouldBypass(username, req) {
		return true
	}
	key := b.key(username, req)
	if key == "" { // An empty key bypasses the blacklist.
		return true
	}
	return b.bl.Allow(key)
}

func (b *authBlacklist) Incr(username string, req *auth.Request, resp *auth.Response) {
	if b.onFailure && resp != nil && resp.Status == auth.StatusOK {
		return
	}
	b.bl.Incr(b.key(username, req))
}

var blacklistCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: "auth_blacklisted_total",
	Help: "Counter of blacklisted entries.",
}, []string{"bl"})

func init() {
	prometheus.MustRegister(blacklistCounter)
}
