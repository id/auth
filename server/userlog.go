package server

import (
	"context"
	"log"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/backend"
	"git.autistici.org/id/usermetadb"
	"git.autistici.org/id/usermetadb/client"
)

type addLogClient interface {
	AddLog(context.Context, string, *usermetadb.LogEntry) error
}

type logFilter struct {
	client addLogClient
}

func newUserActivityLogFilter(config *clientutil.BackendConfig) (*logFilter, error) {
	c, err := client.New(config)
	if err != nil {
		return nil, err
	}
	return &logFilter{c}, nil
}

var userLogTimeout = 30 * time.Second

func (f *logFilter) Filter(user *backend.User, req *auth.Request, resp *auth.Response) *auth.Response {
	if resp.Status != auth.StatusOK {
		return resp
	}

	entry := usermetadb.LogEntry{
		Timestamp:            time.Now(),
		Username:             user.Name,
		Type:                 usermetadb.LogTypeLogin,
		Message:              "successful login",
		Service:              req.Service,
		LoginMethod:          string(resp.Mechanism),
		LoginAuthenticatorID: resp.AuthenticatorID,
		DeviceInfo:           req.DeviceInfo,
	}

	// Make the log RPC in the background, no need to wait for it to complete.
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), userLogTimeout)
		defer cancel()
		if err := f.client.AddLog(ctx, user.Shard, &entry); err != nil {
			log.Printf("usermetadb.AddLog error for %s: %v", user.Name, err)
		}
	}()

	return resp
}
