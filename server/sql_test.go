package server

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"git.autistici.org/id/auth"
)

var (
	testConfigTemplateWithSimpleDB = `---
backends:
  sql:
    driver: sqlite3
    db_uri: "%s"
services:
  test:
    backends:
      - backend: sql
        params:
          queries:
            get_user: "SELECT email, password, '' AS totp_secret, '' AS shard FROM users WHERE email = ?"
` + webAuthnConfigStr

	testConfigTemplateWithFullDB = `---
backends:
  sql:
    driver: sqlite3
    db_uri: "%s"
services:
  test:
    backends:
      - backend: sql
        params:
          queries:
            get_user: "SELECT email, password, totp_secret, '' AS shard FROM users WHERE name = ?"
            get_user_u2f: "SELECT public_key, key_handle FROM u2f_registrations WHERE name = ?"
            get_user_asp: "SELECT '', service, password FROM asps WHERE name = ?"
  no2fa:
    ignore_2fa: true
    backends:
      - backend: sql
        params:
          queries:
            get_user: "SELECT email, password, totp_secret, '' AS shard FROM users WHERE name = ?"
            get_user_asp: "SELECT '', service, password FROM asps WHERE name = ?"
  force2fa:
    enforce_2fa: true
    backends:
      - backend: sql
        params:
          queries:
            get_user: "SELECT email, password, totp_secret, '' AS shard FROM users WHERE name = ?"
            get_user_asp: "SELECT '', service, password FROM asps WHERE name = ?"
  interactive:
    challenge_response: true
    backends:
      - backend: sql
        params:
          queries:
            get_user: "SELECT email, password, totp_secret, '' AS shard FROM users WHERE name = ?"
            get_user_groups: "SELECT group_name FROM group_membership WHERE name = ?"
            get_user_u2f: "SELECT public_key, key_handle FROM u2f_registrations WHERE name = ?"
            get_user_asp: "SELECT '', service, password FROM asps WHERE name = ?"
` + webAuthnConfigStr
)

func withTestDB(t testing.TB, schema string) (func(), string) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	dbPath := filepath.Join(dir, "test.db")

	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		t.Fatalf("sql.Open: %v", err)
	}
	tx, _ := db.Begin()
	tx.Exec(schema)
	if err := tx.Commit(); err != nil {
		t.Fatalf("sql error: %v", err)
	}
	db.Close()

	return func() {
		os.RemoveAll(dir)
	}, dbPath
}

func TestAuthServer_SQL_SimpleSchema(t *testing.T) {
	// Test a minimal database schema.
	cleanup, dbPath := withTestDB(t, `
CREATE TABLE users (
    email text,
    password text
);
CREATE UNIQUE INDEX users_idx ON users(email);
INSERT INTO users (email, password) VALUES (
    'test@example.com', '$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b'
);
`)
	defer cleanup()

	conf := fmt.Sprintf(testConfigTemplateWithSimpleDB, dbPath)
	s := createTestServer(t, map[string]string{
		"config.yml": conf,
	})
	defer s.Close()

	client := &clientAdapter{s.srv}
	resp, err := client.Authenticate(context.Background(), &auth.Request{
		Service:  "test",
		Username: "test@example.com",
		Password: []byte("password"),
	})
	if err != nil {
		t.Fatalf("Authenticate: %v", err)
	}
	if resp.Status != auth.StatusOK {
		t.Fatalf("authentication failed: %v", resp.Status)
	}
}

func TestAuthServer_SQL(t *testing.T) {
	// Full schema that can run standard authentication tests.
	cleanup, dbPath := withTestDB(t, `
CREATE TABLE users (
    name text,
    email text,
    totp_secret text,
    password text
);
CREATE UNIQUE INDEX users_name_idx ON users(name);
CREATE TABLE group_membership (
    name text,
    group_name text
);
CREATE INDEX group_membership_idx ON group_membership(name);
CREATE TABLE u2f_registrations (
    name text,
    key_handle blob,
    public_key blob
);
CREATE INDEX u2f_registrations_idx ON u2f_registrations(name);
CREATE TABLE asps (
    name text,
    service text,
    password text
);
CREATE INDEX asp_idx ON asps(name);
INSERT INTO users (name, email, totp_secret, password) VALUES (
    'testuser', 'testuser@example.com', NULL, '$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b'), (
    '2fauser', '2fauser@example.com', 'O32OBVS5BL5EAPB5', '$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b'), (
    'aspuser', 'aspuser@example.com', NULL, '$s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b');
INSERT INTO group_membership (name, group_name) VALUES (
    'testuser', 'group1'), (
    '2fauser', 'group2');
INSERT INTO u2f_registrations (name, key_handle, public_key) VALUES (
    '2fauser', X'25ca255c0e8a6a88a13bc56ec52ba0b424f98f287eea516e5972e41403def2cf6ab33c5332f0c0b499fc826620f6e18efa49a381aa7587496572196aaa30a92b', X'0498ee4565cd348031cf36ee3549b63b5ea23b5e7ea6f297e7cccaeba99983d185110fb94fa6455c82d3e5c8d0be10be71308d76062fb5fa50d3ea8228048f0037');
INSERT INTO asps (name, service, password) VALUES (
    'aspuser', 'test', '$s$16384$8$1$5b8b0dc22ec2bc1029b06d4856a8b471$6c7257652e3982d8729b760885887b18d4a28966f9b2364041ddada8bea297db'), (
    'aspuser', 'force2fa', '$s$16384$8$1$5b8b0dc22ec2bc1029b06d4856a8b471$6c7257652e3982d8729b760885887b18d4a28966f9b2364041ddada8bea297db');
`)
	defer cleanup()

	conf := fmt.Sprintf(testConfigTemplateWithFullDB, dbPath)
	s := createTestServer(t, map[string]string{
		"config.yml": conf,
	})
	defer s.Close()

	runBackendTest(t, s.srv)
	runAuthenticationTest(t, &clientAdapter{s.srv})
}
