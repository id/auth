package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"flag"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	common "git.autistici.org/ai3/go-common"
	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/lineproto"
	"github.com/coreos/go-systemd/v22/activation"
	"github.com/coreos/go-systemd/v22/daemon"
	"golang.org/x/sync/errgroup"
)

var (
	socketPath              = flag.String("socket", "", "`path` to the UNIX socket to listen on")
	systemdSocketActivation = flag.Bool("systemd-socket", false, "use SystemD socket activation")
	upstreamAddr            = flag.String("upstream", "", "upstream address (host:port)")
	sslCert                 = flag.String("ssl-cert", "", "SSL certificate `file`")
	sslKey                  = flag.String("ssl-key", "", "SSL private key `file`")
	sslCA                   = flag.String("ssl-ca", "", "SSL CA `file` (enables client TLS)")
)

func buildTLSConfig() (*tls.Config, error) {
	if *sslCA == "" {
		return nil, nil
	}
	cas, err := common.LoadCA(*sslCA)
	if err != nil {
		return nil, err
	}
	tlsConf := &tls.Config{
		RootCAs: cas,
	}
	if *sslCert != "" && *sslKey != "" {
		cert, err := tls.LoadX509KeyPair(*sslCert, *sslKey)
		if err != nil {
			return nil, err
		}
		tlsConf.Certificates = []tls.Certificate{cert}
	}
	return tlsConf, nil
}

type proxyServer struct {
	codec  auth.Codec
	dialer func() (*lineproto.Conn, error)
}

var (
	authCmd = []byte("auth ")
	quitCmd = []byte("quit")
)

func newProxyServer(upstream string, tlsConfig *tls.Config) *proxyServer {
	s := &proxyServer{
		codec: auth.DefaultCodec,
	}
	if tlsConfig != nil {
		s.dialer = func() (*lineproto.Conn, error) {
			c, err := tls.Dial("tcp", upstream, tlsConfig)
			if err != nil {
				return nil, err
			}
			return lineproto.NewConn(c, ""), nil
		}
	} else {
		s.dialer = func() (*lineproto.Conn, error) {
			c, err := net.Dial("tcp", upstream)
			if err != nil {
				return nil, err
			}
			return lineproto.NewConn(c, ""), nil
		}
	}
	return s
}

func (s *proxyServer) ServeLine(ctx context.Context, lw lineproto.LineResponseWriter, line []byte) error {
	if bytes.HasPrefix(line, quitCmd) {
		return lineproto.ErrCloseConnection
	}
	if bytes.HasPrefix(line, authCmd) {
		conn, err := s.dialer()
		if err != nil {
			return err
		}
		defer conn.Close()

		if err := conn.WriteLine(line); err != nil {
			return err
		}
		resp, err := conn.ReadLine()
		if err != nil {
			return err
		}
		return lw.WriteLine(resp)
	}
	return errors.New("syntax error")
}

type genericServer interface {
	Serve() error
	Close()
}

func runServer(ctx context.Context, srv genericServer) error {
	go func() {
		<-ctx.Done()
		if ctx.Err() == context.Canceled {
			srv.Close()
		}
	}()
	daemon.SdNotify(false, "READY=1") // nolint: errcheck
	return srv.Serve()
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	syscall.Umask(007)

	tlsConfig, err := buildTLSConfig()
	if err != nil {
		log.Fatal(err)
	}
	proxySrv := newProxyServer(*upstreamAddr, tlsConfig)
	srv := lineproto.NewLineServer(proxySrv)

	outerCtx, cancel := context.WithCancel(context.Background())
	g, ctx := errgroup.WithContext(outerCtx)

	var servers []genericServer
	if *systemdSocketActivation {
		ll, err := activation.Listeners()
		if err != nil {
			log.Fatal(err)
		}
		for _, l := range ll {
			servers = append(servers, lineproto.NewServer("systemd", l, srv))
		}
	}
	if *socketPath != "" {
		l, err := lineproto.NewUNIXSocketListener(*socketPath)
		if err != nil {
			log.Fatal(err)
		}
		servers = append(servers, lineproto.NewServer("unix", l, srv))
	}

	if len(servers) == 0 {
		log.Fatal("no sockets available for listening")
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	for _, s := range servers {
		func(s genericServer) {
			g.Go(func() error {
				return runServer(ctx, s)
			})
		}(s)
	}

	err = g.Wait()
	if err != nil && err != context.Canceled {
		log.Fatal(err)
	}
}
