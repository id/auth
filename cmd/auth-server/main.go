package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/go-common/tracing"
	"github.com/coreos/go-systemd/v22/activation"
	"github.com/coreos/go-systemd/v22/daemon"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"

	"git.autistici.org/id/auth/lineproto"
	"git.autistici.org/id/auth/server"
)

var (
	configPath              = flag.String("config", "/etc/auth-server/config.yml", "configuration `file`")
	socketPath              = flag.String("socket", "/run/auth/socket", "`path` to the UNIX socket to listen on")
	systemdSocketActivation = flag.Bool("systemd-socket", false, "use SystemD socket activation")
	httpAddr                = flag.String("http-addr", "", "if not nil, bind an HTTP server to this `addr` for Prometheus metrics")
	tcpAddr                 = flag.String("addr", "", "listen on this TCP address")
	requestTimeout          = flag.Duration("timeout", 5*time.Second, "timeout for incoming requests")

	sslCert = flag.String("ssl-cert", "", "SSL certificate `file`")
	sslKey  = flag.String("ssl-key", "", "SSL private key `file`")
	sslCA   = flag.String("ssl-ca", "", "SSL CA `file` (requires client certificates)")
	sslACLs = flag.String("ssl-acl", "", "SSL access control lists (comma-separated list of regexps matching CN)")
)

func usage() {
	fmt.Fprintf(os.Stderr, `authserv - a simple authentication server
Usage: authserv [<OPTIONS>]

Known options:
`)
	flag.PrintDefaults()
}

type genericServer interface {
	Serve() error
	Close()
}

func runServer(ctx context.Context, srv genericServer) error {
	go func() {
		<-ctx.Done()
		if ctx.Err() == context.Canceled {
			srv.Close()
		}
	}()
	daemon.SdNotify(false, "READY=1") // nolint: errcheck
	return srv.Serve()
}

type metricsServer struct {
	*http.Server
}

func newMetricsServer() *metricsServer {
	h := http.NewServeMux()
	h.Handle("/metrics", promhttp.Handler())
	return &metricsServer{
		Server: &http.Server{
			Addr:         *httpAddr,
			Handler:      h,
			ReadTimeout:  10 * time.Second,
			IdleTimeout:  30 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
	}
}

func (s *metricsServer) Serve() error {
	err := s.Server.ListenAndServe()
	if err == http.ErrServerClosed {
		err = nil
	}
	return err
}

func (s *metricsServer) Close() {
	s.Server.Close() // nolint: errcheck
}

func main() {
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	syscall.Umask(007)

	config, err := server.LoadConfig(*configPath)
	if err != nil {
		log.Fatalf("could not load configuration: %v", err)
	}

	authSrv, err := server.NewServer(config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	tracing.Init()

	outerCtx, cancel := context.WithCancel(context.Background())
	g, ctx := errgroup.WithContext(outerCtx)

	if *httpAddr != "" {
		g.Go(func() error {
			return runServer(ctx, newMetricsServer())
		})
	}

	srv := lineproto.NewLineServer(server.NewSocketServer(authSrv))
	srv.RequestTimeout = *requestTimeout

	var servers []genericServer
	// For legacy reasons, systemd-socket takes precedence over socket.
	if *systemdSocketActivation {
		ll, err := activation.Listeners()
		if err != nil {
			log.Fatal(err)
		}
		for _, l := range ll {
			servers = append(servers, lineproto.NewServer("systemd", l, srv))
		}
	} else if *socketPath != "" {
		l, err := lineproto.NewUNIXSocketListener(*socketPath)
		if err != nil {
			log.Fatal(err)
		}
		servers = append(servers, lineproto.NewServer("unix", l, srv))
	}
	if *tcpAddr != "" {
		var llsrv lineproto.Handler = srv
		if *sslCert != "" {
			var acls []string
			if *sslACLs != "" {
				acls = strings.Split(*sslACLs, ",")
			}
			var err error
			llsrv, err = newTLSServer(
				llsrv,
				&serverutil.TLSServerConfig{
					Cert: *sslCert,
					Key:  *sslKey,
					CA:   *sslCA,
				},
				acls,
			)
			if err != nil {
				log.Fatal(err)
			}
		}

		l, err := net.Listen("tcp", *tcpAddr)
		if err != nil {
			log.Fatal(err)
		}
		servers = append(servers, lineproto.NewServer("tcp", l, llsrv))
	}

	if len(servers) == 0 {
		log.Fatal("no sockets available for listening")
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	for _, s := range servers {
		func(s genericServer) {
			g.Go(func() error {
				return runServer(ctx, s)
			})
		}(s)
	}

	err = g.Wait()
	if err != nil && err != context.Canceled {
		log.Fatal(err)
	}
}
