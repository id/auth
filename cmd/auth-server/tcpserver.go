package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"log"
	"regexp"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/id/auth/lineproto"
)

type tlsServer struct {
	h      lineproto.Handler
	config *tls.Config

	acl []*regexp.Regexp
}

func newTLSServer(h lineproto.Handler, config *serverutil.TLSServerConfig, acls []string) (*tlsServer, error) {
	// Use serverutil.TLSServerConfig to build a tls.Config.
	tlsConf, err := config.TLSConfig()
	if err != nil {
		return nil, err
	}

	s := &tlsServer{
		h:      h,
		config: tlsConf,
	}
	for _, acl := range acls {
		rx, err := regexp.Compile(acl)
		if err != nil {
			return nil, fmt.Errorf("acl error: %v", err)
		}
		s.acl = append(s.acl, rx)
	}

	return s, nil
}

func (s *tlsServer) aclCheck(certs []*x509.Certificate) error {
	if len(s.acl) == 0 {
		return nil
	}
	if len(certs) < 1 {
		return errors.New("no certificate was provided")
	}
	for _, acl := range s.acl {
		if acl.MatchString(certs[0].Subject.CommonName) {
			return nil
		}
	}
	return errors.New("access denied")
}

func (s *tlsServer) ServeConnection(conn *lineproto.Conn) {
	sconn := tls.Server(conn, s.config)
	defer sconn.Close()

	if err := sconn.Handshake(); err != nil {
		log.Printf("tls handshake error: %v", err)
		return
	}

	if err := s.aclCheck(sconn.ConnectionState().PeerCertificates); err != nil {
		log.Printf("acl error: %v", err)
		return
	}

	s.h.ServeConnection(lineproto.NewConn(sconn, conn.ServerName))
}
