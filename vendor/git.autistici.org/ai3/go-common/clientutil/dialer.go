// +build go1.9

package clientutil

import (
	"context"
	"net"
	"time"
)

func netDialContext(addr string, connectTimeout time.Duration) func(context.Context, string, string) (net.Conn, error) {
	dialer := &net.Dialer{
		Timeout:   connectTimeout,
		KeepAlive: 30 * time.Second,
		DualStack: true,
	}
	return func(ctx context.Context, net string, _ string) (net.Conn, error) {
		return dialer.DialContext(ctx, net, addr)
	}
}
