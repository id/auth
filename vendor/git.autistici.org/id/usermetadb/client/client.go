package client

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/usermetadb"
)

// Client for the user-meta-server API.
type Client interface {
	CheckDevice(context.Context, string, string, *usermetadb.DeviceInfo) (bool, error)
	AddLog(context.Context, string, *usermetadb.LogEntry) error
	GetUserDevices(context.Context, string, string) ([]*usermetadb.MetaDeviceInfo, error)
	GetUserLogs(context.Context, string, string, int, int) ([]*usermetadb.LogEntry, error)
	SetLastLogin(context.Context, string, *usermetadb.LastLoginEntry) error
	GetLastLogin(context.Context, string, string, string) ([]*usermetadb.LastLoginEntry, error)
}

type udbClient struct {
	be clientutil.Backend
}

// New returns a new Client with the given Config.
func New(config *clientutil.BackendConfig) (Client, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &udbClient{be}, nil
}

func (c *udbClient) CheckDevice(ctx context.Context, shard, username string, dev *usermetadb.DeviceInfo) (bool, error) {
	req := usermetadb.CheckDeviceRequest{
		Username:   username,
		DeviceInfo: dev,
	}
	var resp usermetadb.CheckDeviceResponse

	err := c.be.Call(ctx, shard, "/api/check_device", &req, &resp)
	return resp.Seen, err
}

func (c *udbClient) AddLog(ctx context.Context, shard string, entry *usermetadb.LogEntry) error {
	req := usermetadb.AddLogRequest{Log: entry}
	return c.be.Call(ctx, shard, "/api/add_log", &req, nil)
}

func (c *udbClient) GetUserDevices(ctx context.Context, shard, username string) ([]*usermetadb.MetaDeviceInfo, error) {
	req := usermetadb.GetUserDevicesRequest{Username: username}
	var resp usermetadb.GetUserDevicesResponse
	err := c.be.Call(ctx, shard, "/api/get_user_devices", &req, &resp)
	return resp.Devices, err
}

func (c *udbClient) GetUserLogs(ctx context.Context, shard, username string, maxDays, limit int) ([]*usermetadb.LogEntry, error) {
	req := usermetadb.GetUserLogsRequest{
		Username: username,
		MaxDays:  maxDays,
		Limit:    limit,
	}
	var resp usermetadb.GetUserLogsResponse
	err := c.be.Call(ctx, shard, "/api/get_user_logs", &req, &resp)
	return resp.Results, err
}

func (c *udbClient) SetLastLogin(ctx context.Context, shard string, entry *usermetadb.LastLoginEntry) error {
	req := usermetadb.SetLastLoginRequest{LastLogin: entry}
	return c.be.Call(ctx, shard, "/api/set_last_login", &req, nil)
}

func (c *udbClient) GetLastLogin(ctx context.Context, shard string, username string, service string) ([]*usermetadb.LastLoginEntry, error) {
	req := usermetadb.GetLastLoginRequest{
		Username: username,
		Service:  service,
	}
	var resp usermetadb.GetLastLoginResponse

	err := c.be.Call(ctx, shard, "/api/get_last_login", &req, &resp)
	return resp.Results, err
}
